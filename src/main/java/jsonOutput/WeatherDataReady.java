package jsonOutput;

public class WeatherDataReady {

    private WeatherData weatherDataJson;

    private String cityName;
    private String country;
    private Double temperature;
    private Integer humidity;
    private Double windSpeed;
    private Integer windDegree;

    public static final class Builder {
        private String cityName;
        private String country;
        private Double temperature;
        private Integer humidity;
        private Double windSpeed;
        private Integer windDegree;

        public Builder cityName(String cityName) {
            this.cityName = cityName;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder temperature(Double temperature) {
            this.temperature = temperature;
            return this;
        }

        public Builder humidity(Integer humidity) {
            this.humidity = humidity;
            return this;
        }

        public Builder windSpeed(Double windSpeed) {
            this.windSpeed = windSpeed;
            return this;
        }

        public Builder windDegree(Integer windDegree) {
            this.windDegree = windDegree;
            return this;
        }

        public WeatherDataReady build() {
            WeatherDataReady weatherDataReady = new WeatherDataReady();
            weatherDataReady.cityName = this.cityName;
            weatherDataReady.country = this.country;
            weatherDataReady.temperature = this.temperature;
            weatherDataReady.humidity = this.humidity;
            weatherDataReady.windSpeed = this.windSpeed;
            weatherDataReady.windDegree = this.windDegree;
            return weatherDataReady;
        }
    }
}
